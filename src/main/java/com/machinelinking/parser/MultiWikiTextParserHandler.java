/*
 * JSONpedia - Convert any MediaWiki document to JSON.
 *
 * Written in 2014 by Michele Mostarda <mostarda@fbk.eu>.
 *
 * To the extent possible under law, the author has dedicated all copyright and related and
 * neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC BY Creative Commons Attribution 4.0 Internationa Public License.
 * If not, see <https://creativecommons.org/licenses/by/4.0/legalcode>.
 */

package com.machinelinking.parser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows to multiply {@link WikiTextParserHandler}s messages.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class MultiWikiTextParserHandler implements WikiTextParserHandler {

    private List<WikiTextParserHandler> handlers = new ArrayList<WikiTextParserHandler>();

    public MultiWikiTextParserHandler() {}

    public void add(WikiTextParserHandler h) {
        handlers.add(h);
    }

    public void remove(WikiTextParserHandler h) {
        handlers.remove(h);
    }

    @Override
    public void beginDocument(URL document) {
        for(WikiTextParserHandler handler : handlers) {
            handler.beginDocument(document);
        }
    }

    @Override
    public void parseWarning(String msg, ParserLocation location) {
        for(WikiTextParserHandler handler : handlers) {
            handler.parseWarning(msg, location);
        }
    }

    @Override
    public void parseError(Exception e, ParserLocation location) {
        for(WikiTextParserHandler handler : handlers) {
            handler.parseError(e, location);
        }
    }

    @Override
    public void paragraph() {
        for(WikiTextParserHandler handler : handlers) {
            handler.paragraph();
        }
    }

    @Override
    public void section(String title, int level) {
        for(WikiTextParserHandler handler : handlers) {
            handler.section(title, level);
        }
    }

    @Override
    public void beginReference(String label) {
        for (WikiTextParserHandler handler : handlers) {
            handler.beginReference(label);
        }
    }

    @Override
    public void endReference(String label) {
        for (WikiTextParserHandler handler : handlers) {
            handler.endReference(label);
        }
    }

    @Override
    public void beginLink(URL url) {
        for (WikiTextParserHandler handler : handlers) {
            handler.beginLink(url);
        }
    }

    @Override
    public void endLink(URL url) {
        for (WikiTextParserHandler handler : handlers) {
            handler.endLink(url);
        }
    }

    @Override
    public void beginList() {
        for(WikiTextParserHandler handler : handlers) {
            handler.beginList();
        }
    }

    @Override
    public void listItem(ListType t, int level) {
        for(WikiTextParserHandler handler : handlers) {
            handler.listItem(t, level);
        }
    }

    @Override
    public void endList() {
        for(WikiTextParserHandler handler : handlers) {
            handler.endList();
        }
    }

    @Override
    public void beginTemplate(TemplateName name) {
        for(WikiTextParserHandler handler : handlers) {
            handler.beginTemplate(name);
        }
    }

    @Override
    public void endTemplate(TemplateName name) {
        for(WikiTextParserHandler handler : handlers) {
            handler.endTemplate(name);
        }
    }

    @Override
    public void beginTable() {
        for(WikiTextParserHandler handler : handlers) {
            handler.beginTable();
        }
    }

    @Override
    public void headCell(int row, int col) {
        for(WikiTextParserHandler handler : handlers) {
            handler.headCell(row, col);
        }
    }

    @Override
    public void bodyCell(int row, int col) {
        for(WikiTextParserHandler handler : handlers) {
            handler.bodyCell(row, col);
        }
    }

    @Override
    public void endTable() {
        for(WikiTextParserHandler handler : handlers) {
            handler.endTable();
        }
    }

    @Override
    public void beginTag(String node, Attribute[] attributes) {
        for(WikiTextParserHandler handler : handlers) {
            handler.beginTag(node, attributes);
        }
    }

    @Override
    public void endTag(String node) {
        for(WikiTextParserHandler handler : handlers) {
            handler.endTag(node);
        }
    }

    @Override
    public void inlineTag(String node, Attribute[] attributes) {
        for(WikiTextParserHandler handler : handlers) {
            handler.inlineTag(node, attributes);
        }
    }

    @Override
    public void commentTag(String comment) {
        for(WikiTextParserHandler handler : handlers) {
            handler.commentTag(comment);
        }
    }

    @Override
    public void parameter(String param) {
        for(WikiTextParserHandler handler : handlers) {
            handler.parameter(param);
        }
    }

    @Override
    public void entity(String form, char value) {
        for(WikiTextParserHandler handler : handlers) {
            handler.entity(form, value);
        }
    }

    @Override
    public void var(Var v) {
        for(WikiTextParserHandler handler : handlers) {
            handler.var(v);
        }
    }

    @Override
    public void text(String content) {
        for(WikiTextParserHandler handler : handlers) {
            handler.text(content);
        }
    }

    @Override
    public void italicBold(int level) {
        for (WikiTextParserHandler handler : handlers) {
            handler.italicBold(level);
        }
    }

    @Override
    public void endDocument() {
        for(WikiTextParserHandler handler : handlers) {
            handler.endDocument();
        }
    }

}
