/*
 * JSONpedia - Convert any MediaWiki document to JSON.
 *
 * Written in 2014 by Michele Mostarda <mostarda@fbk.eu>.
 *
 * To the extent possible under law, the author has dedicated all copyright and related and
 * neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC BY Creative Commons Attribution 4.0 Internationa Public License.
 * If not, see <https://creativecommons.org/licenses/by/4.0/legalcode>.
 */

package com.machinelinking.render;

import com.machinelinking.pagestruct.Ontology;
import com.machinelinking.util.DefaultJsonPathBuilder;
import com.machinelinking.util.JSONUtils;
import com.machinelinking.util.JsonPathBuilder;
import com.machinelinking.wikimedia.WikimediaUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of {@link HTMLRender}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class DefaultHTMLRender implements HTMLRender {

    private static final Map<String,String> CONTENT_STYLE = new HashMap<String,String>(){{
        put("class", "content");
    }};

    private static final Map<String,String> PRIMITIVE_NODE_ATTRS = new HashMap<String,String>(){{
        put("class", "root");
    }};

    private static final Map<String,String> TYPE_LABEL_ATTRS = new HashMap<String,String>(){{
        put("class", "type");
    }};

    private static final Map<String,String> OBJECT_NODE_ATTRS = new HashMap<String, String>(){{
        put("class","object");
    }};

    private static final Map<String,String> LIST_NODE_ATTRS = new HashMap<String, String>(){{
        put("class","list");
    }};

    private static final Map<String,String> DEFAULT_RENDER_NODE_ATTRS = new HashMap<String,String>(){{
    }};

    private static final Map<String,String> DEFAULT_RENDER_HIDDEN_NODE_ATTRS = new HashMap<String,String>(){{
        put("class", String.format("%s %s", DEFAULT_RENDER_CLASS, "default-render") );
    }};

    private static final String DEFAULT_RENDER_CLASS = "defaultrender";

    private final Map<String,List<NodeRender>> nodeRenders       = new HashMap<>();
    private final Map<String,KeyValueRender>   keyValueRenders   = new HashMap<>();
    private final List<PrimitiveNodeRender> primitiveNodeRenders = new ArrayList<>();

    private final JsonPathBuilder jsonPathBuilder = new DefaultJsonPathBuilder();

    private final boolean alwaysRenderDefault;

    private JsonContext context;

    public DefaultHTMLRender(boolean alwaysRenderDefault) {
        this.alwaysRenderDefault = alwaysRenderDefault;
    }

    public void processRoot(JsonNode node, HTMLWriter writer) throws IOException {
        jsonPathBuilder.startPath();
        writer.openDocument(getContext().getDocumentTitle());
        processFragment(reorderRoot(node), writer);
        writer.closeDocument();
        writer.flush();
    }

    public void processFragment(JsonNode node, HTMLWriter writer) throws IOException {
        render(getContext(), this, node, writer);
    }

    @Override
    public void addNodeRender(String type, NodeRender render) {
        List<NodeRender> rendersPerType = nodeRenders.get(type);
        if(rendersPerType == null) {
            rendersPerType = new ArrayList<>();
            nodeRenders.put(type, rendersPerType);
        }
        rendersPerType.add(render);
    }

    @Override
    public boolean removeNodeRender(String type) {
        return nodeRenders.remove(type) != null;
    }

    @Override
    public void addKeyValueRender(String key, KeyValueRender render) {
        if(keyValueRenders.containsKey(key)) throw new IllegalArgumentException();
        keyValueRenders.put(key, render);
    }

    @Override
    public boolean removeKeyValueRender(String key) {
        return keyValueRenders.remove(key) != null;
    }

    @Override
    public void addPrimitiveRender(PrimitiveNodeRender render) {
        primitiveNodeRenders.add(render);
    }

    @Override
    public void removePrimitiveRender(PrimitiveNodeRender render) {
        primitiveNodeRenders.remove(render);
    }

    @Override
    public boolean acceptNode(JsonContext context, JsonNode node) {
        return true;
    }

    @Override
    public void render(JsonContext context, RootRender rootRender, JsonNode node, HTMLWriter writer) throws IOException {
        if(node == null) return;
        final JsonNode type = node.get(Ontology.TYPE_FIELD);
        NodeRender targetRender = null;
        if (type != null) {
            final List<NodeRender> rendersPerType = nodeRenders.get(type.asText());
            if (rendersPerType != null) {
                for(NodeRender nodeRender : rendersPerType) {
                    if(nodeRender.acceptNode(context, node)) {
                        targetRender = nodeRender;
                        break;
                    }
                }
            }
        }
        boolean renderFound = targetRender != null;

        // Node metadata.
        if(alwaysRenderDefault) writeNodeMetadata(node, writer);

        // Custom node rendering.
        if(renderFound) {
            try {
                targetRender.render(context, rootRender, node, writer);
            } catch (Exception e) {
                renderFound = false;
            }
        }

        // Default node rendering.
        if(alwaysRenderDefault || !renderFound) {
            renderDefault(!renderFound, context, rootRender, node, writer);
        }

        if(alwaysRenderDefault) writer.closeTag();
    }

    @Override
    public void render(JsonContext context, RootRender rootRender, String key, JsonNode value, HTMLWriter writer)
    throws IOException {
        final KeyValueRender render = keyValueRenders.get(key);
        if(render != null) {
            writer.anchor(key);
            render.render(context, rootRender, key, value, writer);
            return;
        }

        writer.openTag("div", CONTENT_STYLE);
        writer.openTag("span");
        writer.openTag("i");
        writer.text(key);
        writer.closeTag();
        writer.text(":");
        writer.closeTag();
        writer.openTag("span");
        render(context, rootRender, value, writer);
        writer.closeTag();
        writer.closeTag();
    }

    @Override
    public String renderDocument(DocumentContext context, JsonNode rootNode) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final DefaultHTMLWriter writer = new DefaultHTMLWriter( new OutputStreamWriter(baos) );
        this.setContext(rootNode, context);
        this.processRoot(rootNode, writer);
        writer.flush();
        return baos.toString();
    }

    public String renderFragment(DocumentContext context, JsonNode rootNode) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final DefaultHTMLWriter writer = new DefaultHTMLWriter(new OutputStreamWriter(baos));
        this.setContext(rootNode, context);
        this.processFragment(rootNode, writer);
        writer.flush();
        return baos.toString();
    }

    private JsonNode reorderRoot(JsonNode root) {
        ObjectNode newRoot = JSONUtils.getJsonNodeFactory().objectNode();
        Iterator<Map.Entry<String,JsonNode>> iter = root.getFields();
        Map.Entry<String,JsonNode> entry;
        final String pageStructField = Ontology.PAGE_DOM_FIELD;
        while(iter.hasNext()) {
            entry = iter.next();
            if(pageStructField.equals(entry.getKey())) continue;
            newRoot.put(entry.getKey(), entry.getValue());
        }
        newRoot.put(pageStructField, root.get(pageStructField));
        return newRoot;
    }

    private void setContext(final JsonNode root, final DocumentContext documentContext) {
         context = new JsonContext() {
             final URL documentURL = documentContext.getDocumentURL();
             final String documentTitle = WikimediaUtils.getEntityTitle(documentURL.toExternalForm());
             final String lang = documentURL.getHost().split("\\.")[0];
             final String domain = documentURL.getHost();

             @Override
             public DocumentContext getDocumentContext() {
                 return documentContext;
             }

             @Override
             public String getDocumentTitle() {
                 return documentTitle;
             }

             @Override
             public String getLang() {
                 return lang;
             }

             @Override
             public String getDomain() {
                 return domain;
             }

             @Override
             public String getJSONPath() {
                 return jsonPathBuilder.getJsonPath();
             }

             @Override
             public JsonNode getRoot() {
                 return root;
             }

             @Override
             public boolean subPathOf(JsonPathBuilder builder, boolean strict) {
                 return jsonPathBuilder.subPathOf(builder, strict);
             }
         };
    }

    private JsonContext getContext() {
        if(context == null) throw new IllegalStateException();
        return context;
    }

    private void renderDefault(
            boolean isDefault, JsonContext context, RootRender rootRender, JsonNode node, HTMLWriter writer
    ) throws IOException {
                // Begin Default Render.
        writer.openTag("span");
        if (node.isObject()) {
            renderObject(rootRender, node, isDefault, writer);
        } else if (node.isArray()) {
            renderList(rootRender, node, isDefault, writer);
        } else {
            renderPrimitive(node, writer);
        }
        writer.closeTag();
        // End Default Render.
    }

    private void renderObject(RootRender rootRender, JsonNode obj, boolean isDefault, HTMLWriter writer)
    throws IOException {
        jsonPathBuilder.enterObject();
        writer.openTag("div", isDefault ? DEFAULT_RENDER_NODE_ATTRS : DEFAULT_RENDER_HIDDEN_NODE_ATTRS);
        final Iterator<Map.Entry<String,JsonNode>> iter = obj.getFields();
        Map.Entry<String,JsonNode> entry;
        while(iter.hasNext()) {
            entry = iter.next();
            if(Ontology.TYPE_FIELD.equals(entry.getKey())) continue;
            writer.openTag("div", OBJECT_NODE_ATTRS);
            jsonPathBuilder.field(entry.getKey());
            render(getContext(), rootRender, entry.getKey().trim(), entry.getValue(), writer);
            writer.closeTag();
        }
        jsonPathBuilder.exitObject();

        writer.closeTag();
    }

    private void renderList(RootRender rootRender, JsonNode list, boolean isDefault, HTMLWriter writer)
    throws IOException {
        final int size = list.size();
        if(size == 1) {
            jsonPathBuilder.enterArray();
            jsonPathBuilder.arrayElem();
            render(getContext(), rootRender, list.get(0), writer);
            jsonPathBuilder.exitArray();
            return;
        } else if(size == 0) {
            renderPrimitive(list, writer);
            return;
        }

        writer.openTag("div", LIST_NODE_ATTRS);
        writer.openTag("div", isDefault ? DEFAULT_RENDER_NODE_ATTRS : DEFAULT_RENDER_HIDDEN_NODE_ATTRS);
        jsonPathBuilder.enterArray();
        for(int i = 0; i < list.size(); i++) {
            jsonPathBuilder.arrayElem();
            writer.openTag("div");
            render(getContext(), rootRender, list.get(i), writer);
            writer.closeTag();
        }
        jsonPathBuilder.exitArray();
        writer.closeTag();
        writer.closeTag();
    }

    private void renderPrimitive(JsonNode node, HTMLWriter writer) throws IOException {
        for(PrimitiveNodeRender primitiveRender : primitiveNodeRenders) {
            if( primitiveRender.render(getContext(), node, writer) ) return;
        }

        final String primitive = JSONUtils.asPrimitiveString(node);
        if (primitive != null) {
            writer.openTag("span", PRIMITIVE_NODE_ATTRS);
            writer.text(primitive.trim());
            writer.closeTag();
        } else {
            writer.openTag("span");
            writer.text("&lt;empty&gt;");
            writer.closeTag();
        }
    }

    // TODO: should write any metadata available
    private String writeNodeMetadata(JsonNode node, HTMLWriter writer) throws IOException {
        final JsonNode typeNode = node.get(Ontology.TYPE_FIELD);
        final String type;
        if(typeNode == null) {
            type = null;
            writer.openTag("span", new HashMap<String,String>(){{put("title", jsonPathBuilder.getJsonPath());}});
        } else {
            type = typeNode.asText();
            final String name;
            name = "template".equals(type) ? node.get("name").asText() : null;
            writer.openTag("div", new HashMap<String, String>(){{
                put("itemtype", type);
                put("name"    , name);
                put("title", jsonPathBuilder.getJsonPath());
            }});

            writer.openTag("small", TYPE_LABEL_ATTRS);
            writer.text(type);
            writer.closeTag();
        }
        return type;
    }

}
