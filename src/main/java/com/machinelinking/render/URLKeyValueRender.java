/*
 * JSONpedia - Convert any MediaWiki document to JSON.
 *
 * Written in 2014 by Michele Mostarda <mostarda@fbk.eu>.
 *
 * To the extent possible under law, the author has dedicated all copyright and related and
 * neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC BY Creative Commons Attribution 4.0 Internationa Public License.
 * If not, see <https://creativecommons.org/licenses/by/4.0/legalcode>.
 */

package com.machinelinking.render;

import com.machinelinking.util.JSONUtils;
import org.codehaus.jackson.JsonNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class URLKeyValueRender implements KeyValueRender {

    private static final Map<String,String> URLKEY_DIV_ATTR = new HashMap<String,String>(){{
        put("class", "url");
    }};

    @Override
    public void render(JsonContext context, RootRender rootRender, String key, JsonNode value, HTMLWriter writer)
    throws IOException {
        final JsonNode strippedCommentsValue = JSONUtils.stripComments(value);
        final String valueStr = JSONUtils.asPrimitiveString(strippedCommentsValue);
        writer.openTag("div", URLKEY_DIV_ATTR);
        writer.key(key);
        writer.anchor(valueStr, valueStr, false);
        writer.closeTag();
    }

}
