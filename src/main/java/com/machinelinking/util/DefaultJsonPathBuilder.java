/*
 * JSONpedia - Convert any MediaWiki document to JSON.
 *
 * Written in 2014 by Michele Mostarda <mostarda@fbk.eu>.
 *
 * To the extent possible under law, the author has dedicated all copyright and related and
 * neighboring rights to this software to the public domain worldwide.
 * This software is distributed without any warranty.
 *
 * You should have received a copy of the CC BY Creative Commons Attribution 4.0 Internationa Public License.
 * If not, see <https://creativecommons.org/licenses/by/4.0/legalcode>.
 */

package com.machinelinking.util;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * Default implementation of {@link JsonPathBuilder}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class DefaultJsonPathBuilder implements JsonPathBuilder {

    private Stack<Element> stack = new Stack<>();

    @Override
    public void startPath() {
        stack.clear();
    }

    @Override
    public void enterArray() {
        stack.push( new ArrayElement() );
    }

    @Override
    public void arrayElem() {
        try {
            final ArrayElement arrayElement = (ArrayElement) stack.peek();
            arrayElement.incIndex();
        } catch(ClassCastException cce) {
            throw new IllegalStateException("Can be invoked within an array");
        }
    }

    @Override
    public void exitArray() {
        try {
            if (!(stack.pop() instanceof ArrayElement)) {
                throw new IllegalStateException("An array must be open first.");
            }
        } catch (EmptyStackException ese) {
            throw new IllegalStateException("Not open array.");
        }
    }

    @Override
    public void enterObject() {
        stack.push( new ObjectElement() );
    }

    @Override
    public void field(String fieldName) {
        try {
            final ObjectElement objectElement = (ObjectElement) stack.peek();
            objectElement.setField(fieldName);
        } catch (ClassCastException cce) {
            throw new IllegalStateException("Can be invoked within an object");
        }
    }

    @Override
    public void exitObject() {
        try {
            if (!(stack.pop() instanceof ObjectElement)) {
                throw new IllegalStateException("An object must be open first.");
            }
        } catch (EmptyStackException ese) {
            throw new IllegalStateException("Not open object.");
        }

    }

    @Override
    public String getJsonPath() {
        final StringBuilder sb = new StringBuilder();
        sb.append('$');
        for(Element element : stack) {
            sb.append(element.getPathSeparator());
            sb.append(element.toJsonSection()   );
        }
        return sb.toString();
    }

    @Override
    public boolean subPathOf(JsonPathBuilder other, boolean strict) {
        final DefaultJsonPathBuilder otherBuilder = (DefaultJsonPathBuilder) other;
        final int stackSize      = stack.size();
        final int otherStackSize = otherBuilder.stack.size();
        if(strict) {
            if(stackSize != otherStackSize) return false;
        } else {
            if(otherStackSize > stackSize)  return false;
        }
        for(int i = 0; i < otherStackSize; i++) {
            if(! stack.get(i).equals(otherBuilder.stack.get(i)))
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getJsonPath();
    }

    private interface Element {
        String toJsonSection();
        String getPathSeparator();
    }

    private class ArrayElement implements Element {
        private int index = -1;
        void incIndex() {
            index++;
        }

        @Override
        public String toJsonSection() {
            return String.format("[%s]", index == -1 ? "*" : index);
        }

        @Override
        public String getPathSeparator() {
            return "";
        }

        @Override
        public String toString() {
            return toJsonSection();
        }

        @Override
        public int hashCode() {
            return index;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null) return false;
            if(obj == this) return true;
            if(! (obj instanceof ArrayElement)) return false;
            final ArrayElement other = (ArrayElement) obj;
            return index == -1 || other.index == -1 || index == other.index;
        }
    }

    private class ObjectElement implements Element {

        private String field = null;

        void setField(String fieldName) {
            if(fieldName == null || fieldName.trim().length() == 0)
                throw new IllegalArgumentException("Invalid field: " + fieldName);
            field = fieldName;
        }

        @Override
        public String toJsonSection() {
            return field == null ? "" : field;
        }

        @Override
        public String getPathSeparator() {
            return field == null ? "" : ".";
        }

        @Override
        public int hashCode() {
            return field == null ? 0 : field.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (obj == this) return true;
            if (!(obj instanceof ObjectElement)) return false;
            final ObjectElement other = (ObjectElement) obj;
            return field == null ? other.field == null : field.equals(other.field);
        }

        @Override
        public String toString() {
            return toJsonSection();
        }
    }

}